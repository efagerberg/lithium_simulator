﻿using UnityEngine;
using System.Collections;

public class electronCluster : MonoBehaviour {

	public electron[] electrons;
	public Transform prefab;
	
	void Start () 
	{
		electrons = GameObject.FindObjectsOfType<electron> ();
	}
	
	void Update () 
	{
		kickoutElectron();
		takeInElectron();
	}
	
	void kickoutElectron()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
			electrons[0].shell = -1;
		if (Input.GetKeyDown(KeyCode.Alpha2))
			electrons[1].shell = -1;
		if (Input.GetKeyDown(KeyCode.Alpha3))
			electrons[2].shell = -1;
	}
	
	void takeInElectron() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Instantiate (prefab, Vector3.zero, Quaternion.identity);
		}
	}
}
