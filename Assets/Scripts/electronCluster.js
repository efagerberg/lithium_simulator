﻿#pragma strict
var electron1: Transform;
var electron2: Transform;
var electron3: Transform;
var prefab: Transform;
var netCharge: int;


function Start () 
{
	electron1 = GameObject.Find("electron1") as Transform;
	electron2 = GameObject.Find("electron2") as Transform;
	electron3 = GameObject.Find("electron3") as Transform;
}

function Update () 
{
	kickoutElectron();
	takeInElectron();
}

function kickoutElectron()
{
	if (Input.GetKeyDown(KeyCode.Alpha1))
		electron1.GetComponent(electron).shell = -1;
	if (Input.GetKeyDown(KeyCode.Alpha2))
		electron2.GetComponent(electron).shell = -1;
	if (Input.GetKeyDown(KeyCode.Alpha3))
		electron3.GetComponent(electron).shell = -1;
}

function takeInElectron()
{
	if (Input.GetKeyDown(KeyCode.Space))
	{
		Instantiate(prefab,Vector3 (1, 1  , 0), Quaternion.identity);
	}
}