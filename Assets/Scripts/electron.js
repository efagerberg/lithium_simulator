﻿#pragma strict

//constants
var C = 3e8; //meters per second
var ELECTRIC_CONSTANT = 8.854187e-12; //Farads per meter
var PLANCK_CONSTANT = 6.62606957e-34; //meters^2 kg per second; 
var BOHR_RADIUS = 0.53e-10; //meters
var COULOMB_CONSTANT = 9e9; //Newtons meters squared per Coulomb squared
var JOULE_CONVERSION_TO_ELECTRON_VOLTS = 6.24150934e18; //1 joule converted to electron volts

var rotationAxis = Vector3(0, 0, 0); //which axes to rotate around
var rotateAroundObject: Transform;

var shell: int;
var energyLevel: float; //eV
var kineticEnergy: float; //eV
var potentialEnergy: float; // eV
var totalEnergy: float;

var radius: float; //meters
var orbitalRadius: float; //meters
var q = 1.60217657e-19; //coulombs
var velocity: float;
renderer.material.color = Color.yellow;
rigidbody.mass = 9.11e-31; // kg

function Start(){
	calculateRadius();
	calculateOrbitalRadius();
}

function Update() {
	calculateEnergy();
	calculatePotentialEnergy();
	calculateKineticEnergy();
	calculateVelocity();
	calculateTotalEnergy();
	rotate();
}

function shellCheck() {
	return (shell > 0 && shell < 3);
}

function calculateEnergy() {
	energyLevel = -3 * 13.6/Mathf.Pow(shell,2);
}

function calculateRadius() {
	if (!rotateAroundObject)
	{
		radius = -1;
	}
	else
	{
		radius = Vector3.Distance(rotateAroundObject.transform.position, this.gameObject.transform.position);
	}
}

function calculateOrbitalRadius() {
	if (shellCheck())
	{
		orbitalRadius = Mathf.Pow(shell,2) * BOHR_RADIUS;
	}
	else
	{
		orbitalRadius = -1;
	}
}

function calculateVelocity() {
// Add slowdown for demo
	if (Input.GetKey(KeyCode.DownArrow))
	{
		velocity = (shell * PLANCK_CONSTANT)/(10000000 * rigidbody.mass * orbitalRadius);
	}	
	else if (Input.GetKey(KeyCode.UpArrow))
	{
		velocity = (shell * PLANCK_CONSTANT)/ (rigidbody.mass * orbitalRadius);
	}
}

function calculateKineticEnergy()
{
	kineticEnergy = (rigidbody.mass * Mathf.Pow(velocity,2))/2; //joules
	kineticEnergy = kineticEnergy * JOULE_CONVERSION_TO_ELECTRON_VOLTS; // eV
}

function calculatePotentialEnergy()
{
	potentialEnergy = COULOMB_CONSTANT * (Mathf.Pow(q,2)/radius); // joules
	potentialEnergy = potentialEnergy * JOULE_CONVERSION_TO_ELECTRON_VOLTS; // eV
}

function calculateTotalEnergy()
{
	totalEnergy = kineticEnergy + potentialEnergy;
}

function rotate()
{
	if (shellCheck())
	{
		if (rotateAroundObject) {//If true in the inspector orbit <rotateAroundObject>:
    		transform.RotateAround(rotateAroundObject.transform.position,
    		rotationAxis, velocity);
   		}
   	}
   	else if (shell == -1)
   	{
   		if (rotationAxis == Vector3(0,1,0)) 
   			rigidbody.velocity = -transform.forward * velocity;
   			
   		else if (rotationAxis == Vector3(0,1,1)) 
   			rigidbody.velocity = Vector3.Cross(transform.up, transform.forward) * velocity;
   			
   		else	
   			rigidbody.velocity = Vector3.Cross(-transform.up, transform.forward) * velocity;
   			
   		rotateAroundObject = null;
   	}
   	else 
   	{
   		Destroy(this.gameObject);
   	}
} 
