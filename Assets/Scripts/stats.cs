﻿using UnityEngine;
using System.Collections;

public class stats : MonoBehaviour {

	public electron[] electrons;
	public float electronClusterEnergy = 0;

	// Use this for initialization
	void Start () {
		electrons = GameObject.FindObjectsOfType<electron> ();
	}

	void OnGUI() {
		electronClusterEnergy = 0;
		GUI.color = Color.yellow;
		foreach (electron e in electrons) {
			GUILayout.Label(e.name + ":");
			GUILayout.Label("\t velocity: " + e.velocity + " m/s");
			if (e.shellCheck())
				GUILayout.Label("\t shell: " + e.shell);
			GUILayout.Label("\t energy level: " + e.energyLevel + "e/V");
			GUILayout.Label("\t kinetic enery: " + e.kineticEnergy + " e/V");
			GUILayout.Label("\t potential enery: " + e.potentialEnergy + " e/V");
			GUILayout.Label("\t orbital radius: " + e.orbitalRadius + " m");
			electronClusterEnergy += e.totalEnergy;
		}

		GUILayout.Label("Total Energy: " + electronClusterEnergy + " e/V");
	}
}
