﻿using UnityEngine;
using System.Collections;

public class electron : MonoBehaviour {
	public Vector3 rotationAxis = new Vector3(0, 0, 0); //which axes to rotate around
	public GameObject rotateAroundObject;
	
	public int shell;
	public float energyLevel; //eV
	public float kineticEnergy; //eV
	public float potentialEnergy; // eV
	public float totalEnergy; //eV
	public float velocity; //m/s

	
	public float radius; //meters
	public float orbitalRadius; //meters

	// Use this for initialization
	void Start () {
		renderer.material.color = Color.yellow;
		rigidbody.mass = constants.MASS;
		rotateAroundObject = GameObject.Find ("Lithium Atom");
		calculateRadius();
		calculateOrbitalRadius();
	}
	
	// Update is called once per frame
	void Update () {
		calculateEnergy();
		calculatePotentialEnergy();
		calculateKineticEnergy();
		calculateVelocity();
		calculateTotalEnergy();
		rotate();
	}

	public bool shellCheck() {
		return (shell > 0 && shell < 3);
	}
	
	public void calculateEnergy() {
		energyLevel = -3 * 13.6f/Mathf.Pow(shell,2);
	}
	
	public void calculateRadius() {
		if (!rotateAroundObject)
		{
			radius = -1;
		}
		else
		{
			radius = Vector3.Distance(rotateAroundObject.transform.position, this.gameObject.transform.position);
		}
	}
	
	public void calculateOrbitalRadius() {
		if (shellCheck())
		{
			orbitalRadius = Mathf.Pow(shell,2) * constants.BOHR_RADIUS;
		}
		else
		{
			orbitalRadius = -1;
		}
	}
	
	public void calculateVelocity() {
		// Add slowdown for demo
		if (Input.GetKey(KeyCode.DownArrow))
		{
			velocity = (shell * constants.PLANCK_CONSTANT)/(10000000 * rigidbody.mass * orbitalRadius);
		}	
		else if (Input.GetKey(KeyCode.UpArrow))
		{
			velocity = (shell * constants.PLANCK_CONSTANT)/ (rigidbody.mass * orbitalRadius);
		}
	}
	
	public void calculateKineticEnergy()
	{
		kineticEnergy = (rigidbody.mass * Mathf.Pow(velocity,2))/2; //joules
		kineticEnergy = kineticEnergy * constants.JOULE_CONVERSION_TO_ELECTRON_VOLTS; // eV
	}
	
	public void calculatePotentialEnergy()
	{
		potentialEnergy = constants.COULOMB_CONSTANT * (Mathf.Pow(constants.Q,2)/radius); // joules
		potentialEnergy = potentialEnergy * constants.JOULE_CONVERSION_TO_ELECTRON_VOLTS; // eV
	}
	
	public void calculateTotalEnergy()
	{
		totalEnergy = kineticEnergy + potentialEnergy;
	}
	
	public void rotate()
	{
		if (shellCheck())
		{
			if (rotateAroundObject) {//If true in the inspector orbit <rotateAroundObject>:
				transform.RotateAround(rotateAroundObject.transform.position,
				                       rotationAxis, velocity);
			}
		}
		else if (shell == -1)
		{
			if (rotationAxis == new Vector3(0,1,0)) 
				rigidbody.velocity = -transform.forward * velocity;
			
			else if (rotationAxis == new Vector3(0,1,1)) 
				rigidbody.velocity = Vector3.Cross(transform.up, transform.forward) * velocity;
			
			else	
				rigidbody.velocity = Vector3.Cross(-transform.up, transform.forward) * velocity;
			
			rotateAroundObject = null;
		}
		else 
		{
			Destroy(this.gameObject);
		}
	}
}