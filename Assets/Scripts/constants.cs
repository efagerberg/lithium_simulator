﻿using UnityEngine;
using System.Collections;

public static class constants {
	public static float C = 3e8f; //meters per second
	public static float ELECTRIC_CONSTANT = 8.854187e-12f; //Farads per meter
	public static float PLANCK_CONSTANT = 6.62606957e-34f; //meters^2 kg per second; 
	public static float BOHR_RADIUS = 0.53e-10f; //meters
	public static float COULOMB_CONSTANT = 9e9f; //Newtons meters squared per Coulomb squared
	public static float JOULE_CONVERSION_TO_ELECTRON_VOLTS = 6.24150934e18f; //1 joule converted to electron volts
	public static float MASS = 9.11e-31f; // kg
	public static float Q = 1.60217657e-19f; //coulombs
}
