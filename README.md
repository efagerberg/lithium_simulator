Lithium_Simulator
=================

Modern Physics Project

Built in Unity

Purpose
================

Supposed to simulate a lithium atom with 3 electrons, protons, and neutrons.
This is using the bohr model of the atom.

Calculates in realtime, the radius, potential energy, kinetic energy, rest energy,
energy level in shell, shell number, and Velocity

For the future I would like to change this to C# for object orientation

Controls
=================
Up Arrow sets the velocity of an electron to the actual speed

Down Arrow scales velocity of an electron to be observable

Space spawns new electron (not working)

1, 2, 3 Kick out their perspective electrons (directions are a bit wonky)
